﻿namespace API.BusinessLogic.Authentication;

public class AuthenticationTokens
{
    public string AccessToken { get; set; } = null!;
    public string RefreshToken { get; set; } = null!;
}