﻿namespace API.BusinessLogic.Firebase;

public class ElusiveTargetPush
{
    public string Title { get; set; } = null!;
    public string Body { get; set; } = null!;
    public string? IconUrl { get; set; }
    public string? ImageUrl { get; set; }
    public string? MapUrl { get; set; }
}