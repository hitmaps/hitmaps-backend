﻿namespace API.Controllers.RequestModels.Nodes;

public class NoteModel
{
    public string Type { get; set; } = null!;
    public string? Text { get; set; }
}