﻿namespace API.Controllers.RequestModels.Nodes;

public class MoveNodeModel
{
    public decimal Latitude { get; set; }
    public decimal Longitude { get; set; }
}