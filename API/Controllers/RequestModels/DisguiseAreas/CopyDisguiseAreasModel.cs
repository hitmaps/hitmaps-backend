﻿namespace API.Controllers.RequestModels.DisguiseAreas;

public class CopyDisguiseAreasModel
{
    public int OriginalDisguise { get; set; }
    public int TargetDisguise { get; set; }
}