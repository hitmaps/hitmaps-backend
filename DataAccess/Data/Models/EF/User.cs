﻿namespace DataAccess.Data.Models.EF;

public class User
{
    public int Id { get; set; }
    public ulong DiscordId { get; set; }
}