﻿namespace DataAccess.Data.Models.EF;

public class NodeToMissionVariant
{
    public int NodeId { get; set; }
    public int VariantId { get; set; }
}